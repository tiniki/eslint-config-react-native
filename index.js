module.exports = {
	parser: 'babel-eslint',
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
	},
	plugins: [
		'react-native',
	],
	env: {
		'react-native/react-native': true,
	},
	extends: [
		'airbnb',
	],
	settings: {
		react: {
			version: 'detect',
		},
		propWrapperFunctions: [
			"forbidExtraProps",
		],
	},
	rules: {
		'no-unused-vars': 'warn',
		'import/prefer-default-export': 'off',
		'react/no-unused-prop-types': 'warn',
		'import/no-extraneous-dependencies': 'off',
		'indent': [ 'warn', 'tab', { 'SwitchCase': 1 } ],
		'react/jsx-indent': [ 'warn', 'tab' ],
		'react/jsx-indent-props': [ 'warn', 'tab' ],
		'no-tabs': [ 'warn', { 'allowIndentationTabs': true } ],
		'semi': [ 'warn', 'never' ],
		'react/destructuring-assignment': 'off',
		'react/jsx-filename-extension': [ 'error', { 'extensions': [ '.js', '.jsx' ] } ],
		'react/jsx-one-expression-per-line': [ 'warn', { 'allow': 'single-child' }],
		'comma-dangle': [
			'warn',
			{
				'arrays': 'always-multiline',
				'objects': 'always-multiline',
				'imports': 'always-multiline',
				'exports': 'always-multiline',
				'functions': 'ignore'
			}
		],
		'object-curly-newline': 'off',
		'no-unused-expressions': [ 'warn', { 'allowShortCircuit': true, 'allowTernary': true }],
		'max-len': [ 'warn', 120 ],
		'dot-notation': 'warn',
		'jsx-quotes': [ 'error', 'prefer-single' ],
		'spaced-comment': 'warn',
		'react/forbid-prop-types': 'off',
		'react/require-default-props': 'off',
		'no-underscore-dangle': 'off',
		'camelcase': 'off',
	},
}
