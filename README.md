# eslint-config-react-native-tik

## Installation

```
yarn add --dev eslint @tiniki/eslint-config
```

*Note: We're using `yarn` to install deps. Feel free to change commands to use `npm` 3+ and `npx` if you like*

## Usage

Add to your eslint config (`.eslintrc`, or `eslintConfig` field in `package.json`):

```json
{
    "extends": "@tiniki"
}
```
